﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using PhnxScanner.Cache.Redis;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Azure
{
    public class AuthorizeToken : Attribute,IAsyncActionFilter
    {
        private static string _issuer = string.Empty;
        private static ICollection<SecurityKey> _signingKeys = null;
        private static DateTime _stsMetadataRetrievalTime = DateTime.MinValue;
        private static string scopeClaimType = "http://schemas.microsoft.com/identity/claims/scope";
        public RedisCache<KeyVaultModel> _cache { get; set; }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            _cache = new RedisCache<KeyVaultModel>();
            var dbSettings = _cache.GetCacheItem("scannerCache");
            AzureCredentialsSettings azureCredentialsSettings=null;
            if (dbSettings != null)
            {
                azureCredentialsSettings = dbSettings.Value.AzureCredentialsSettings;
            }

            const string AUTHKEY = "authorization";
            var headers = context.HttpContext.Request.Headers;
            if (headers.ContainsKey(AUTHKEY))
            {
                bool isAuthorized = await Validate(headers[AUTHKEY], azureCredentialsSettings);
                if (!isAuthorized)
                    context.Result = new UnauthorizedResult();
                else
                    await next();
            }
            else
                context.Result = new UnauthorizedResult();
        }
        public async Task<bool> Validate(string jwtToken, AzureCredentialsSettings azureCredentialsSettings)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken cancellationToken = cts.Token;
            string[] tokenArray = jwtToken.Split(' ');
            jwtToken = tokenArray[1];
            string authority = "https://login.microsoftonline.com/tfp/"+ azureCredentialsSettings.Tenant+ "/"+ azureCredentialsSettings.Policy + "/v2.0";
            HttpRequestMessage request = new HttpRequestMessage();
            if (jwtToken == null)
            {
                return false;   
            }

            string issuer;
            ICollection<SecurityKey> signingKeys;
            try
            {
                // The issuer and signingKeys are cached for 24 hours. They are updated if any of the conditions in the if condition is true.
                if (DateTime.UtcNow.Subtract(_stsMetadataRetrievalTime).TotalHours > 24
                    || string.IsNullOrEmpty(_issuer)
                    || _signingKeys == null)
                {
                    // Get tenant information that's used to validate incoming jwt tokens
                    string stsDiscoveryEndpoint = $"{authority}/.well-known/openid-configuration";
                    var configManager = new ConfigurationManager<OpenIdConnectConfiguration>(stsDiscoveryEndpoint, new OpenIdConnectConfigurationRetriever());
                    var config = await configManager.GetConfigurationAsync(cancellationToken);
                    _issuer = config.Issuer;
                    _signingKeys = config.SigningKeys;
                    _stsMetadataRetrievalTime = DateTime.UtcNow;
                }

                issuer = _issuer;
                signingKeys = _signingKeys;
            }
            catch (Exception)
            {
               return false;
            }

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            TokenValidationParameters validationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.FromMinutes(5),
                RequireSignedTokens = true,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ValidateAudience = false,
                ValidIssuers = new[] { issuer, $"{issuer}" },
                ValidateIssuerSigningKey = true,
                IssuerSigningKeys = signingKeys
            };

            try
            {
                // Validate token.
                SecurityToken validatedToken = new JwtSecurityToken();
                ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(jwtToken, validationParameters, out validatedToken);
                // Set the ClaimsPrincipal on the current thread.
                //Thread.CurrentPrincipal = claimsPrincipal;

                // If the token is scoped, verify that required permission is set in the scope claim.
                if (claimsPrincipal.FindFirst(scopeClaimType) != null && claimsPrincipal.FindFirst(scopeClaimType).Value != "Read")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (SecurityTokenValidationException)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

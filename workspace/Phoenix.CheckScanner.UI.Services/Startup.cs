﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Application
 * package  Startup
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddSessionStateTempDataProvider();
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(20);
                options.Cookie.HttpOnly = true;
            });
            services.AddAuthentication();

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //   .AddJwtBearer(jwtOptions =>
            //   {

            //       jwtOptions.TokenValidationParameters = new TokenValidationParameters
            //       {
            //           // Clock skew compensates for server time drift.
            //           // We recommend 5 minutes or less:
            //           ClockSkew = TimeSpan.FromMinutes(5),
            //           RequireSignedTokens = true,
            //           // Ensure the token hasn't expired:
            //           RequireExpirationTime = true,
            //           ValidateLifetime = true,
            //           // Ensure the token audience matches our audience value (default true):
            //           ValidateAudience = false,
            //           // Ensure the token was issued by a trusted authorization server (default true):
            //           ValidateIssuer = false,
            //           ValidIssuer = "https://phoenixiam.onmicrosoft.com/oauth2/default",
            //           ValidateIssuerSigningKey = true
            //       };
            //       jwtOptions.Authority = $"https://login.microsoftonline.com/tfp/{Configuration["AzureAdB2C:Tenant"]}/{Configuration["AzureAdB2C:Policy"]}/v2.0";
            //       //jwtOptions.Authority = $"https://login.microsoftonline.com/phoenixiam.onmicrosoft.com/v2.0"; ///.well-known/openid-configuration";

            //       jwtOptions.Audience = Configuration["AzureAdB2C:ClientId"];
            //       jwtOptions.Events = new JwtBearerEvents
            //       {

            //           OnAuthenticationFailed = AuthenticationFailed
            //       };
            //   });

            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Scanner API", Description = "Scanner API" });
            });
            services.AddCors(options =>
            {
                options.AddPolicy("ScannerCors", b =>
                {
                    b.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                });
            });
            services.Configure<KeyVaultSettings>(Configuration.GetSection("KeyVaultSettings"));
            services.Configure<AzureCredentialsSettings>(Configuration.GetSection("AzureCredentialsSettings"));
            services.Configure<TransactionAPISettings>(Configuration.GetSection("LegacyTransactionAPISettings"));
            //services.Configure<VerificationAPISettings>(Configuration.GetSection("VerificationAPISettings"));
            services.AddScoped<IKeyVaultSecret, KeyVaultSecret>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            env.EnvironmentName = EnvironmentName.Development;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/Swagger.json", "scanner API");
            });
            app.UseSession();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=authentication}/{id?}");
            });
            // Shows UseCors with CorsPolicyBuilder.
            app.UseCors("ScannerCors");
            app.UseExceptionHandler(
               options =>
               {
                   options.Run(
                   async context =>
                   {
                       context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                       context.Response.ContentType = "text/html";
                       var ex = context.Features.Get<IExceptionHandlerFeature>();
                       if (ex != null)
                       {
                           var err = $"<h1>Error: {ex.Error.Message}</h1>{ex.Error.StackTrace }";
                           await context.Response.WriteAsync(err).ConfigureAwait(false);
                       }                       
                   });
               }
              );
        }

        private Task AuthenticationFailed(AuthenticationFailedContext arg)
        {
            // For debugging purposes only!
            var s = $"AuthenticationFailed: {arg.Exception.Message } {arg.Exception.StackTrace } {arg.Exception.InnerException.Message }  ";
            arg.Response.ContentLength = s.Length;
            arg.Response.Body.Write(Encoding.UTF8.GetBytes(s), 0, s.Length);
            return Task.FromResult(0);
        }
    }
}

﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Error handinling Middleware
 * package  ErrorHandlingMiddleware
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Phoenix.CheckScanner.Connectors.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        /*
       * Default constructor to iniliaze the RequestDelegate instance
       *
       * name   ErrorHandlingMiddleware
       * access public
       * author VCI <info@vericheck.net>
       *
       * return void
       */
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /*
       * Function Invokes the HttpContext
       *
       * param context contains the all the properties of HttpContext, 
       * Request object is used during activity and error logging.
       *
       * name   Invoke
       * access public
       * author VCI <info@vericheck.net>
       *
       * return Task
      */
        public async Task Invoke(HttpContext context, RequestModel requestModel /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
                await HandleExceptionAsync(context);
            }
        }

        /*
      * Function Invokes the HttpContext
      *
      * param context contains the all the properties of HttpContext,       
      *
      * name   HandleExceptionAsync
      * access public
      * author VCI <info@vericheck.net>
      *
      * return string
     */
        private static Task HandleExceptionAsync(HttpContext context)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var result = JsonConvert.SerializeObject(new { errors = "Somthing went wrong." });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}

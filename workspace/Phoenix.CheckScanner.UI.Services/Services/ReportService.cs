﻿using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.Connectors.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Services
{
    public class ReportService
    {
        ReportsConnector _reprotConnector;

        /*
       * Default constructor to iniliaze the reportConnector instance
       *
       * name   ReportService
       * access public
       * author VCI <info@vericheck.net>
       *
       * return void
       */
        public ReportService(ReportsConnector reportConnector)
        {
            _reprotConnector = reportConnector;
        }

        /*
      * Function gets the transaction report based on the parameters passed.
      *
      * param Obj documentDBService is used to access the services of DocumentDB, reportFilters contains all the properties based on which the Transaction  report is generated,
      * Request object is used during activity and error logging.
      *
      * name   GetTransactionReport
      * access public
      * author VCI <info@vericheck.net>
      *
      * return TransactionModel 
     */
        public IEnumerable<TransactionModel> GetTransactionReport(DocumentDBConnector<TransactionModel> documentDbService, ReportFilters reportFilters,RequestModel request)
        {
            return _reprotConnector.GetTransactionReport(documentDbService, reportFilters,request);
        }
    }
}

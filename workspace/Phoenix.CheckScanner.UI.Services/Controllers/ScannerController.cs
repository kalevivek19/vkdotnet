﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Controller
 * package  ScannerController
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services.Azure;
using Phoenix.CheckScanner.UI.Services.Helpers;
using Phoenix.CheckScanner.UI.Services.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    /*
    * Scanner Controller works to gather the scanner related requests like Adding/Editing or 
    * deleting the scanner information from the merchant
    *
    * name     ScannerController
    * category Controller
    * package  Scanner Controller
    * author   VCI <info@vericheck.net>
    * license  Copyright 2018 VeriCheck | All Rights Reserved
    * version  GIT:
    * link     https://www.vericheck.com/docs/
    */
    [Produces("application/json")]
    [Route("scanners")]
    [EnableCors("ScannerCors")]
    [AuthorizeToken]
    public class ScannerController : Controller
    {
        private readonly IKeyVaultSecret _keyVaultSecret;
        readonly ErrorHelper errorHelper = new ErrorHelper();
        /*
        * Parameterized constructor to iniliaze the keyVaultSecret instance
        *
        * param keyVaultSecret
        * 
        * name   ScannerController
        * access public
        * author VCI <info@vericheck.net>
        *
        * return void
        */
        public ScannerController(IKeyVaultSecret keyVaultSecret)
        {
            _keyVaultSecret = keyVaultSecret;          
        }

        /*
       * Function gets all scanner settings based on the parameters passed.
       *
       * param merchant Id
       *
       * name   GetAllScannerSettings
       * access public
       * author VCI <info@vericheck.net>
       *
       * return IEnumerable of Scanner
      */
        /// <summary>
        /// Get merchant specific scanner settings
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpGet("getAllScannerSettings")]
        public IEnumerable<Scanner> GetAllScannerSettings(string merchantId)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            activityLogHelper.LogActivity(Request, "Get", "Scanner", "Getting all scanner settings");

            ScannerService scannerService = new ScannerService(ConnectorServicesFactory.GetTS240Instance());
            var _documentDbService = DocumentDbServicesFactory<Scanner>.GetDocumentDbInstance();

            IEnumerable<Scanner> scanner = scannerService.GetAllScannerSettings(_documentDbService, VCSConstants.Scanner, merchantId);
            return scanner;
        }

        /*
       * Function gets the scanner instance
       *
       * param id
       *
       * name   Get
       * access public
       * author VCI <info@vericheck.net>
       *
       * return Scanner
      */
        /// <summary>
        /// Get scanner setting to edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "Get")]
        public Scanner Get(string id)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            activityLogHelper.LogActivity(Request, "Get", "Scanner", "Getting scanner settings");
            ScannerService scannerService = new ScannerService(ConnectorServicesFactory.GetTS240Instance());
            var _documentDbService = DocumentDbServicesFactory<Scanner>.GetDocumentDbInstance();
            Scanner scanner = scannerService.GetScannerSettings(_documentDbService, id);
            return scanner;
        }

        /*
      * Function posts the scanner object
      *
      * param scanner
      *
      * name   Post
      * access public
      * author VCI <info@vericheck.net>
      *
      * return string
     */
        /// <summary>
        /// Save scanner settings
        /// </summary>
        /// <param name="scanner"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> Post([FromBody]Scanner scanner)
        {
            //Below code is used to log the actions performed with the help of ActivityLogHelper
            string status = string.Empty;
            if (scanner != null)
            {
                Scanner oldScanner = null;
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                if (string.IsNullOrEmpty(scanner.Id))
                {
                    oldScanner = Get(scanner.Id);
                }
                activityLogHelper.LogActivity(Request, "Post", "Scanner", "Posintg scanner data", scanner, oldScanner);

                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Scanner", "Posintg scanner data");


                if (ModelState.IsValid)
                {
                    ScannerService scannerService = new ScannerService(ConnectorServicesFactory.GetTS240Instance());
                    var _documentDbService = DocumentDbServicesFactory<Scanner>.GetDocumentDbInstance();
                    scanner.Type = VCSConstants.Scanner;
                   
                    status = await scannerService.SaveScannerSettings(_documentDbService, scanner, request);
                }
                else
                {
                    var errorList = errorHelper.GetErrors(ModelState, request);
                    return JsonConvert.SerializeObject(errorList);
                }
            }
            return status;
        }

        /*
        * Function deletes the scanner object
        *
        * param id
        *
        * name   Delete
        * access public
        * author VCI <info@vericheck.net>
        *
        * return string
       */
        /// <summary>
        /// Delete scanner settings
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<string> Delete(string id)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            activityLogHelper.LogActivity(Request, "Delete", "Scanner", "Deleting scanner settings");

            ScannerService scannerService = new ScannerService(ConnectorServicesFactory.GetTS240Instance());
            var _documentDbService = DocumentDbServicesFactory<Scanner>.GetDocumentDbInstance();
            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Delete", "Scanner", "Deleting scanner settings");

            string status = await scannerService.DeleteScannerSetting(_documentDbService, id, request);
            return status;
        }
    }
}

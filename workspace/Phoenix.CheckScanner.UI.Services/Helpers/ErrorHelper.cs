﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Helper
 * package  ErrorHelper
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Phoenix.CheckScanner.Connectors.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors.Contracts;

namespace Phoenix.CheckScanner.UI.Services.Helpers
{
    public class ErrorHelper
    {

        /*
        * Function gets the errors from modelstate object.
        *
        * param modelState of ModelStateDictionary,
        * RequestModel object is used during activity and error logging.
        *
        * name   GetImageContentFromAzureByBlobId
        * access public
        * author VCI <info@vericheck.net>
        *
        * return list of TransactionModel
       */
        public List<ErrorModel> GetErrors(ModelStateDictionary modelState, RequestModel requestModel)
        {
           
            List<ErrorModel> ErrorList = new List<ErrorModel>();
            try
            {
              
                foreach (var modelStateKey in modelState.Keys)
                {
                    var modelStateVal = modelState[modelStateKey];
                    foreach (var error in modelStateVal.Errors)
                    {
                        string[] errorArray = error.ErrorMessage.Split('-');
                        if (errorArray.Count() > 1)
                        {
                            ErrorModel model = new ErrorModel()
                            {
                                ErrorMessage = errorArray[0],
                                ErrorCode = errorArray[1],
                                FieldName = modelStateKey

                            };
                            ErrorList.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
            }
            return ErrorList.ToList();
        }
    }
}

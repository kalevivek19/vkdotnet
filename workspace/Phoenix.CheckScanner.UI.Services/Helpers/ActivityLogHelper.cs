﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Helper
 * package  ActivityLogHelper
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Http;
using Microsoft.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Helpers
{
    public class ActivityLogHelper : AzureServicesConnector
    {
        // Connection String for the namespace can be obtained from the Azure portal under the 
        // 'Shared Access policies' section.
        const string ServiceBusConnectionString = "Endpoint=sb://scannerservicebus.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=aGP/u3XWaU1fz2xs09Wj7t2tzlvlhe+q5UsjowoQngM=";
        const string QueueName = "activity_log";
        static IQueueClient queueClient;

        /*
        * Function logs the activity
        *
        * param Request object, HttpMethod, module, description, new and old scanner object.    
        *
        * name   LogActivity
        * access public
        * author VCI <info@vericheck.net>
        *
        * return void
       */
        public void LogActivity(HttpRequest Request,string httpMethod, string module, string desc, Scanner newScanner =null,Scanner oldScanner = null)
        {
            try
            {
                queueClient = new QueueClient(ServiceBusConnectionString, QueueName);

                string newData = (newScanner != null) ? Newtonsoft.Json.JsonConvert.SerializeObject(newScanner) : string.Empty;
                string existingData = (oldScanner != null) ? Newtonsoft.Json.JsonConvert.SerializeObject(oldScanner) : string.Empty;
              
                ActivityLog activityLog = new ActivityLog()
                {
                    User_id = "",
                    User_name = "",
                    User_agent = Request.Headers["User-Agent"].ToString(),
                    Remote_ip_address = Request.Headers["X-Original-For"].ToString(),
                    Http_method = httpMethod,
                    Current_url = Request.Headers["Host"].ToString(),
                    Module = module,
                    Description = desc,
                    Old_value = existingData,
                    New_value = newData,
                    Created_at = DateTime.UtcNow.ToString()
                };

                string messageBody = Newtonsoft.Json.JsonConvert.SerializeObject(activityLog);                
                var message = new Microsoft.Azure.ServiceBus.Message(Encoding.UTF8.GetBytes(messageBody));
               
                // Send the message to the queue
                queueClient.SendAsync(message);                
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }


        /*
        * Function prepares the request model
        *
        * param userAgent, IpAddress, currentURL, HttpMethod, module and description.    
        *
        * name   PrepareRequestModel
        * access public
        * author VCI <info@vericheck.net>
        *
        * return RequestModel
       */
        public RequestModel PrepareRequestModel(string userAgent, string IpAddress, string currentURL, string HttpMethod, string module, string desc)
        {
            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel requestModel = new RequestModel()
            {
                User_agent = userAgent,
                Remote_ip_address = IpAddress,
                Current_url = currentURL,
                Http_method = HttpMethod,
                Module = module,
                Description = desc
            };

            return requestModel;
        }

        /*
        * Function validate the azure credentials
        *
        * param azureCredetials    
        *
        * name   ValidateAzureCredentials
        * access public
        * author VCI <info@vericheck.net>
        *
        * return string
       */
        public override string ValidateAzureCredentials(AzureCredentialsSettings azureCredetials)
        {
            throw new NotImplementedException();
        }

       

        /*
     * Function logs the error
     *
     * param ex object and requestmodel 
     *
     * name   LogError
     * access public
     * author VCI <info@vericheck.net>
     *
     * return void
    */
        public override void LogError(Exception ex, RequestModel request = null)
        {
            throw new NotImplementedException();
        }

        public override Task<string> GetClientToken(AzureCredentialsSettings _azureCredentialsSettings)
        {
            throw new NotImplementedException();
        }
    }
}

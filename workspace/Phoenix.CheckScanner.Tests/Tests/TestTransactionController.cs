﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services;
using Phoenix.CheckScanner.UI.Services.Helpers;

namespace Phoenix.CheckScanner.Tests
{
    [TestFixture]
    public class TestTransactionController
    {
        Mock<IOptions<TransactionAPISettings>> _legacyTransactionAPISettings = new Mock<IOptions<TransactionAPISettings>>();
        TransactionAPISettings legacyTransactionAPISettings = new TransactionAPISettings()
        {
            //https://sandbox.usaepay.com/api/v2/transactions
            Url = "https://usaepay.com/api/v2/transactions",
            //UserName = "api_dEeSxZWdxKVBHzncJtiZXcNNjKPAY7ydocjc",
            SourceKey = "3VT4AYAy7RAoUb9VKe79h0pUt17Wy3I9",
            Pin = "1234",
           // SourceKey = "P11PON_ENTER_SOURCE_KEY_HERE_KSQr1VT81"
        };
       
        [Test]
        public void MakeTransaction_When_TransactionModelList_Empty()
        {
            TransactionController transactionController = new TransactionController(_legacyTransactionAPISettings.Object);
            List<TransactionModel> TransactionModelList = new List<TransactionModel>();
            transactionController.ControllerContext = new ControllerContext();
            transactionController.ControllerContext.HttpContext = new DefaultHttpContext();
            transactionController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
            Task<IEnumerable<TransactionModel>> result = transactionController.MakeTransaction(TransactionModelList);
            Assert.AreEqual(null, result.Result);
        }

        [Test]
        public void MakeTransaction_When_TransactionModelList_Null()
        {
            TransactionController transactionController = new TransactionController(_legacyTransactionAPISettings.Object);
            List<TransactionModel> TransactionModelList = null;
            transactionController.ControllerContext = new ControllerContext();
            transactionController.ControllerContext.HttpContext = new DefaultHttpContext();
            transactionController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
            Task<IEnumerable<TransactionModel>> result = transactionController.MakeTransaction(TransactionModelList);
            Assert.AreEqual(null, result.Result);
        }

        [Test]
        public void MakeTransaction_When_ModelStateValid()
        {
            _legacyTransactionAPISettings.Setup(ap => ap.Value).Returns(legacyTransactionAPISettings);
            TransactionController transactionController = new TransactionController(_legacyTransactionAPISettings.Object);
            List<TransactionModel> TransactionModelList = new List<TransactionModel>();
            TransactionModel model = new TransactionModel();
            model.Amount = "20";
            model.Transaction_Gateway = GatewayTypes.USA;
            model.Merchant_Id = "mer_kKhmPLweebXmg5QjEQgcCNDWcAcdGyHrvbzq";
            //model.Merchant_Id = "d4d1fa30-18b2-4c08-84d2-96f0f1b73f6e";
            TransactionModelList.Add(model);
            transactionController.ControllerContext = new ControllerContext();
            transactionController.ControllerContext.HttpContext = new DefaultHttpContext();
            transactionController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
            Task<IEnumerable<TransactionModel>> result = transactionController.MakeTransaction(TransactionModelList);
            Assert.AreEqual(null, result.Result);
        }

        [Test]
        public void TestErrorHelperzx_WithNoHyphenInErrors()
        {
            ErrorHelper errorHelper = new ErrorHelper();
            TransactionController transactionController = new TransactionController(_legacyTransactionAPISettings.Object);
            Merchant merchant = new Merchant();
            RequestModel request = new RequestModel();
            transactionController.ModelState.AddModelError("fakeError", "fakeError");
            List<ErrorModel> result = errorHelper.GetErrors(transactionController.ModelState, request);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void TestErrorHelperzx_WithMoreThanOneHyphenInErrors()
        {
            ErrorHelper errorHelper = new ErrorHelper();
            TransactionController transactionController = new TransactionController(_legacyTransactionAPISettings.Object);
            Merchant merchant = new Merchant();
            RequestModel request = new RequestModel();
            transactionController.ModelState.AddModelError("fakeError", "fakeError1-fakeError2");
            List<ErrorModel> result = errorHelper.GetErrors(transactionController.ModelState, request);
            Assert.AreEqual(1, result.Count);
        }

        [Test]
        public void UploadImageToBlob_ReturnsNull_WhenTransactionNotValid()
        {
            BlobsHelper blobsHelper = new BlobsHelper("test");
            Merchant merchant = new Merchant() { };
            //  blobsHelper.UploadImageToBlob(merchant);
            //merchant.Transactions = new TransactionModel[2];
        }
    }
}

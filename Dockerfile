From microsoft/dotnet:2.1.302-sdk


WORKDIR /app

COPY /workspace/Phoenix.CheckScanner.UI.Services/out .

# set up network
ENV ASPNETCORE_URLS http://+:80

ENTRYPOINT ["dotnet", "Phoenix.CheckScanner.UI.Services.dll"]